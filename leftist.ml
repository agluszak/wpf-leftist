(* Autor: Andrzej Gluszak
Reviewer: Mikolaj Grzywacz *)

type 'a queue =
  (* lewe poddrzewo * wartosc * prawa wysokosc * prawe poddrzewo *)
  | Tree of 'a queue * 'a * int * 'a queue
  | Null

exception Empty

(* konstruktory *)
let empty = Null

let singleton x = Tree (Null, x, 1, Null)
(* selektory *)
let left_subtree t = match t with
  | Null -> raise Empty
  | Tree(l,_,_,_) -> l

let right_subtree t = match t with
  | Null -> raise Empty
  |  Tree(_,_,_,r) -> r

let value t = match t with
  | Null -> raise Empty
  | Tree(_,a,_,_) -> a

let height t = match t with
  | Null -> 0
  | Tree(_,_,h,_) -> h

let is_empty t = t = Null

(* modyfikatory *)
let rec join t1 t2 =
  if is_empty t1 then t2 else if is_empty t2 then t1
  (* pierwsze powinno byc to z mniejszym korzeniem *)
  else if value t2 < value t1 then join t2 t1
  (* laczymy prawe poddrzewo pierwszego i cale drugie *)
  else let t3 = join (right_subtree t1) t2 in
  (* na prawe poddrzewo wynikowe wybieramy to o nizszej prawej wysokosci *)
    let h1 = height (left_subtree t1) and h2 = height t3 in
    if h2 <= h1 then
      (* konstrujemy drzewo i uaktualniamy wysokosci *)
      Tree(left_subtree t1, value t1, h2 + 1, t3)
    else
      (* analogicznie w przypadku zamiany poddrzew miejscami *)
      Tree(t3, value t1, h1 + 1, left_subtree t1)

let delete_min t =
  if is_empty t then raise Empty else
  (* zwracamy korzen i drzewo powstale ze sklejenia poddrzew *)
  (value t, join (left_subtree t) (right_subtree t))

(* sklejamy istniejace drzewo z drzewem jednoelementowym *)
let add x t = join (singleton x) t
